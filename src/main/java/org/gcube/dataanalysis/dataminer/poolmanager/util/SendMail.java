package org.gcube.dataanalysis.dataminer.poolmanager.util;

import static org.gcube.common.authorization.client.Constants.authorizationService;
import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.dataanalysis.dataminer.poolmanager.clients.configuration.DMPMClientConfiguratorManager;
import org.gcube.dataanalysis.dataminer.poolmanager.util.exception.EMailException;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendMail {

	private static final Logger logger = LoggerFactory.getLogger(SendMail.class);
	
	private static final String WRITE_MESSAGE_ADDRESS_PATH = "2/messages/write-message?gcube-token=";
	private static final String USER_ROLES_ADDRESS_PATH = "2/users/get-usernames-by-role?role-name=DataMiner-Manager&gcube-token=";
	private static final String SOCIAL_SERVICE_QUERY_CONDITION = "$resource/Profile/ServiceName/text() eq 'SocialNetworking'";
	private static final String SOCIAL_SERVICE_URI = "jersey-servlet", JSON_MIME_TYPE = "application/json";
	private static final String DMPOOLMANAGER_SERVICE_QUERY_CONDITION = "$resource/Profile/Name/text() eq 'DMPoolManager'";

	
	private String socialServiceAddress;
	private String applicationToken;

	public SendMail() {

	}

	public void sendNotification(String subject, String body) throws EMailException {
		logger.debug("SendNotification");
		logger.debug("Notification Subject: " + subject);
		logger.debug("Notification Body: " + body);

		applicationToken = retrieveApplicationToken();

		retrieveSocialService();

		String postBody = createPostBody(subject, body);

		sendPostRequest(postBody);

	}

	private String createPostBody(String subject, String body) throws EMailException {
		try {
			List<String> recipientsList = getRecipients();
			if (recipientsList == null || recipientsList.isEmpty()) {
				logger.error("Invalid recipient list: " + recipientsList);
				throw new EMailException("Unable to send email notification. Invalid recipient list:" + recipientsList);
			}

			// {"subject": "subject-content", "body": "body-content",
			// "recipients":[{"id":"userid"}]}

			JSONObject data = new JSONObject();
			data.put("subject", subject);
			data.put("body", body);

			JSONArray recipients = new JSONArray();
			for (String recipient : recipientsList) {
				JSONObject d = new JSONObject();
				d.put("id", recipient);
				recipients.put(d);
			}
			data.put("recipients", recipients);

			logger.debug("Post Body: " + data);
			return data.toString();

		} catch (EMailException e) {
			throw e;
		} catch (Throwable e) {
			logger.error("Error creating the notification body: " + e.getLocalizedMessage(), e);
			throw new EMailException(e);

		}

	}

	private String retrieveApplicationToken() throws EMailException {
		try {
			logger.info("Retrieve Application Token");
			SimpleQuery query = queryFor(ServiceEndpoint.class);
			query.addCondition(DMPOOLMANAGER_SERVICE_QUERY_CONDITION);
			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
			List<ServiceEndpoint> resources = client.submit(query);
			if (resources.isEmpty()) {
				logger.error("No services resource found on IS!");
			} else {
				logger.debug("Retrieved: " + resources.get(0));
			}

			Group<AccessPoint> accessPoints = resources.get(0).profile().accessPoints();
			if (!accessPoints.isEmpty()) {
				Iterator<AccessPoint> iterator = accessPoints.iterator();
				AccessPoint ap = iterator.next();
				Group<Property> props = ap.properties();
				if (!props.isEmpty()) {
					Iterator<Property> iteratorProps = props.iterator();
					Property p = iteratorProps.next();
					String applicationToken = StringEncrypter.getEncrypter().decrypt(p.value());
					logger.debug("Application token found: " + applicationToken);
					logger.info("Application Token retrieved");
					return applicationToken;

				} else {
					String error = "DMPoolManager application token not found in service resource on IS!";
					logger.error(error);
					throw new EMailException(error);
				}
			} else {
				String error = "DMPoolManager invalid service resource on IS!";
				logger.error(error);
				throw new EMailException(error);
			}

		} catch (Throwable e) {
			logger.error("DMPoolManager application token not found: " + e.getLocalizedMessage(), e);
			throw new EMailException("DMPoolManager application token not found: " + e.getLocalizedMessage(), e);
		}

	}

	private void retrieveSocialService() throws EMailException {
		try {
			SimpleQuery query = queryFor(GCoreEndpoint.class);
			query.addCondition(SOCIAL_SERVICE_QUERY_CONDITION);
			DiscoveryClient<GCoreEndpoint> client = clientFor(GCoreEndpoint.class);
			List<GCoreEndpoint> resources = client.submit(query);
			socialServiceAddress = resources.get(0).profile().endpointMap().get(SOCIAL_SERVICE_URI).uri().toString();
			logger.info("Retrieved Social Service Address: " + socialServiceAddress);
			if (socialServiceAddress == null || socialServiceAddress.isEmpty()) {
				throw new EMailException(
						"Unable to send email notification. Invalid address in GCoreEndpoint resource on IS.");
			}
		} catch (EMailException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw e;
		} catch (Throwable e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new EMailException(e);
		}

	}

	private String username(String token) throws ObjectNotFound, Exception {
		AuthorizationEntry entry = authorizationService().get(token);
		logger.debug(entry.getClientInfo().getId());
		return entry.getClientInfo().getId();
	}

	private void sendPostRequest(String postBody) throws EMailException {

		try {
			logger.info("Execute Post Body:" + postBody);

			StringBuilder requestMessageBuilder = new StringBuilder(socialServiceAddress);

			if (!socialServiceAddress.endsWith("/"))
				requestMessageBuilder.append('/');

			requestMessageBuilder.append(WRITE_MESSAGE_ADDRESS_PATH);
			logger.info("Execute Post Request: " + requestMessageBuilder.toString());
			requestMessageBuilder.append(applicationToken);
			String endpoint = requestMessageBuilder.toString();

			// Send the request
			URL url = new URL(endpoint);
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("Accept", JSON_MIME_TYPE);
			conn.setRequestProperty("Content-Type", JSON_MIME_TYPE);
			conn.setDoOutput(true);

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(postBody);
			writer.flush();

			// Get the response
			StringBuffer answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();

			logger.debug("Operation completed");
			String response = answer.toString();
			logger.info("Notification Response: " + response);
			checkResponse(response);

		} catch (EMailException e) {
			throw e;
		} catch (MalformedURLException e) {
			logger.error("Invalid URL: " + e.getLocalizedMessage(), e);
			throw new EMailException(e);
		} catch (IOException e) {
			logger.error("Error in the IO process: " + e.getLocalizedMessage(), e);
			throw new EMailException(e);
		} catch (Throwable e) {
			logger.error("Error executing post:" + e.getLocalizedMessage(), e);
			throw new EMailException(e);
		}

	}

	private void checkResponse(String response) throws EMailException {
		if (response == null) {
			logger.error("Invalid notification response: " + response);
			throw new EMailException();
		} else {
			try {
				JSONObject res = new JSONObject(response);
				boolean success = res.getBoolean("success");
				if (!success) {
					String message = res.getString("message");
					logger.error("Error in send email notification: " + message);
					throw new EMailException("Error in send email notification: " + message);
				}

			} catch (JSONException e) {
				logger.error("Invalid notification response: " + response);
				throw new EMailException(e);
			}

		}
	}

	private List<String> getRecipients() {
		try {
			List<String> recipients = new ArrayList<String>();
			String dataMinerManagers = retrieveDataMinerManagers();
			logger.debug("Retrieved DataMiner Managers: " + dataMinerManagers);
			if (dataMinerManagers != null && !dataMinerManagers.isEmpty()) {
				JSONObject obj = new JSONObject(dataMinerManagers);
				JSONArray data = obj.getJSONArray("result");
				if (data != null) {
					for (int i = 0; i < data.length(); i++) {
						recipients.add(data.getString(i));
					}
				}
			} else {
				logger.info("Use the default admins how workaround ");
				List<String> defaultManagers = DMPMClientConfiguratorManager.getInstance().getDefaultAdmins();
				recipients.addAll(defaultManagers);
			}
			recipients.add(this.username(SecurityTokenProvider.instance.get()));

			logger.info("Retrieved Recipients: " + recipients);
			return recipients;
		} catch (Exception e) {
			logger.error("Error retrieving recipients: " + e.getLocalizedMessage(), e);
			logger.info("Use the default admins how workaround ");
			return DMPMClientConfiguratorManager.getInstance().getDefaultAdmins();
		}

	}

	private String retrieveDataMinerManagers() throws Exception {
		// Try to retrieve a url like this:
		// https://api.d4science.org/social-networking-library-ws/rest/2/users/get-usernames-by-role?role-name=DataMiner-Manager&gcube-token=xxx-xxxx-xxxx-xxx

		StringBuilder requestMessageBuilder = new StringBuilder(socialServiceAddress);

		if (!socialServiceAddress.endsWith("/"))
			requestMessageBuilder.append('/');

		requestMessageBuilder.append(USER_ROLES_ADDRESS_PATH);
		logger.info("Request Admins Url: " + requestMessageBuilder.toString());
		// SecurityTokenProvider.instance.get()
		requestMessageBuilder.append(applicationToken);

		String requestAdminsUrl = requestMessageBuilder.toString();

		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet getReq = new HttpGet(requestAdminsUrl);
		getReq.setHeader("accept", JSON_MIME_TYPE);
		getReq.setHeader("content-type", JSON_MIME_TYPE);
		logger.info("Response: " + EntityUtils.toString(client.execute(getReq).getEntity()));

		return EntityUtils.toString(client.execute(getReq).getEntity());

	}

}
