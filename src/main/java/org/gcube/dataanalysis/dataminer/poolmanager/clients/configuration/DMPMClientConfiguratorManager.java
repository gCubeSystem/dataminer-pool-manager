package org.gcube.dataanalysis.dataminer.poolmanager.clients.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.dataanalysis.dataminer.poolmanager.clients.ScopedCacheMap;
import org.gcube.dataanalysis.dataminer.poolmanager.clients.configuration.ConfigurationImpl.CONFIGURATIONS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNException;

public class DMPMClientConfiguratorManager {
	private final Logger logger=LoggerFactory.getLogger(DMPMClientConfiguratorManager.class);
	private Properties defaultAdmins;

	static DMPMClientConfiguratorManager instance;

	private ScopedCacheMap cacheMap;

	private DMPMClientConfiguratorManager() {
		cacheMap = new ScopedCacheMap();
		defaultAdmins = new Properties();

		try {
			defaultAdmins.load(this.getClass().getResourceAsStream("/default.admins"));
			logger.debug("Default users successfully loaded");
		} catch (Exception e) {
			logger.error("Unable to get default users", e);
		}
	}

	private ClientConfigurationCache getCurrentCache() {
		String currentScope = ScopeProvider.instance.get();
		logger.debug("Current scope = " + currentScope);
		logger.debug("Getting current configuration cache");
		ClientConfigurationCache cache = cacheMap.get(currentScope);

		if (cache == null) {
			logger.debug("Cache not created yet, creating...");
			cache = new ClientConfigurationCache();
			cacheMap.put(currentScope, cache);

		}

		return cache;

	}

	public static DMPMClientConfiguratorManager getInstance() {
		if (instance == null)
			instance = new DMPMClientConfiguratorManager();

		return instance;
	}

	public Configuration getProductionConfiguration() {
		return new ConfigurationImpl(CONFIGURATIONS.PROD, getCurrentCache());
	}

	public Configuration getStagingConfiguration() {
		return new ConfigurationImpl(CONFIGURATIONS.STAGE, getCurrentCache());
	}

	public List<String> getDefaultAdmins() {
		List<String> admins = new ArrayList<String>();

		if (defaultAdmins == null || defaultAdmins.isEmpty()) {
			admins.add("statistical.manager");
		} else {
			Iterator<Object> keys = this.defaultAdmins.keySet().iterator();

			while (keys.hasNext()) {
				String key = (String) keys.next();
				admins.add(defaultAdmins.getProperty(key));
			}

		}

		this.logger.debug("Default admins list: " + admins);
		return admins;

	}

	public static void main(String[] args) throws IOException, SVNException {
		DMPMClientConfiguratorManager a = new DMPMClientConfiguratorManager();
		ScopeProvider.instance.set("/gcube/devsec/devVRE");
		// SecurityTokenProvider.instance.set("xxx-xxx-xxx-xxx");

		System.out.println("RESULT 1" + a.getStagingConfiguration().getSVNCRANDepsList());
		System.out.println("RESULT 2" + a.getProductionConfiguration().getRepository());
		System.out.println("RESULT 3" + a.getStagingConfiguration().getSVNRepository().getPath());
		// System.out.println(a.getRepo());
		// System.out.println(a.getAlgoRepo());
		// System.out.println(a.getSVNRepo());

	}
}
