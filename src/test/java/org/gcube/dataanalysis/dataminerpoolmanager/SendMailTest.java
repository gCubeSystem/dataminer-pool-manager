package org.gcube.dataanalysis.dataminerpoolmanager;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.dataanalysis.dataminer.poolmanager.util.SendMail;

public class SendMailTest {

	public static void main(String[] args) {
		try {

			// NotificationHelper nh = new NotificationHelper();

			SendMail sm = new SendMail();

			ScopeProvider.instance.set("/gcube/devNext/NextNext");
			SecurityTokenProvider.instance.set("xxxx-xxxx-xxx");
			// System.out.println(sm.getRootToken());
			// sm.getGenericResourceByName("");

			//ScopeProvider.instance.set("/gcube/devsec/devVRE");
			//SecurityTokenProvider.instance.set("xxxx-xxxx-xxxx");
			
			// sm.sendNotification(nh.getFailedSubject(), nh.getFailedBody("test
			// failed"));
			// sm.username(SecurityTokenProvider.instance.get());
			// sm.retrieveAdminRole();
			// sm.getAdminRoles();
			sm.sendNotification("Test DMPoolManager Notification", "This is only a test please cancel this message.");
			// System.out.println(sm.getAdmins());
			// sm.sendNotification("test", "test");
			// System.out.println(sm.getSocialService());
		} catch (Exception e) {
			System.out.println("Error in sent mail: "+e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
}
