package org.gcube.dataanalysis.dataminerpoolmanager;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.Iterator;
import java.util.List;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

public class ApplicationTokenTest {

	private static final String DMPOOLMANAGER_SERVICE_QUERY_CONDITION = "$resource/Profile/Name/text() eq 'DMPoolManager'";

	public static void main(String[] args) {
		ScopeProvider.instance.set("/gcube/devsec/devVRE");
		SecurityTokenProvider.instance.set("xxxx-xxxx-xxx");

		decryptToken();
	}

	private static void decryptToken() {
		try {

			SimpleQuery query = queryFor(ServiceEndpoint.class);
			query.addCondition(DMPOOLMANAGER_SERVICE_QUERY_CONDITION);
			DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
			List<ServiceEndpoint> resources = client.submit(query);
			if (resources.isEmpty()) {
				System.out.println("No services resource found on IS!");
				
			} else {
				System.out.println("Retrieved: " + resources.get(0));
			}
			
			
			Group<AccessPoint> accessPoints = resources.get(0).profile().accessPoints();
			if (!accessPoints.isEmpty()) {
				Iterator<AccessPoint> iterator = accessPoints.iterator();
				AccessPoint ap = iterator.next();
				Group<Property> props = ap.properties();
				if (!props.isEmpty()) {
					Iterator<Property> iteratorProps = props.iterator();
					Property p = iteratorProps.next();
					String applicationToken = StringEncrypter.getEncrypter().decrypt(p.value());
					System.out.println("Application token found: " + applicationToken);

				} else {
					System.out.println("No application token found in service resource on IS!");
				}
			} else {
				System.out.println("Invalid service resource on IS!");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
