# Changelog


## [v2.8.0-SNAPSHOT]

- Updated jsch to 0.1.55
- Updated to gcube-smartgears-bom: 2.5.1-SNAPSHOT
- Updated pom.xml to include configuration resources [#26192]

## [v2.7.1] - 2022-04-06

- Updated to gcube-smartgears-bom.2.1.1 [#23133]


## [v2.7.0] - 2020-04-16

- Updated to new Social Networking API [#19081]
- Added application token for send notifications [#19186]


## [v2.6.0] - 2019-12-11

- Updated to Git and Jenkins
- Knime 4.1 added [#18190]


## [v2.5.0] - 2019-01-11

- SVN parameters get from IS
- Python3.6 added [#12742]


## [v2.4.0] - 2018-11-01

- Notifies the 'conflicts' on SVN


## [v2.3.0] - 2018-08-01

- Log information also if the verification of SVN dependencies fails


## [v2.2.0] - 2018-06-01

- Improvements and bugs fixed/SVN-UTF8 compliant/Installation in Production ghost added
- Dynamic per-VRE configuration throght the information system


## [v2.1.0] - 2018-05-01

- New configuration file added


## [v2.0.0] - 2018-01-01

- Second Release


## [v1.0.0] - 2017-11-01

- First Release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
